Global $MV  =  "Minecraft 1.8.9"
Global $ST = 25
Global $HK = "x"
Global $MC = 0
Global $Stop = 0
Global $a = ""
Global $AmountC = 0
Global $AmountCB = 0
Global $AmountN = 0
Global $AmountNW = 0
Global $TurnOff = 0

Func MB($MA,$MD,$H,$S)
	ConsoleWrite($MD)
   ControlSend($MV, "", "", "{" & $MD & " down}")
   If $S = 1 Then
	  ControlSend($MV, "", "", "{CTRLDOWN}")
   EndIf
   If $H = 1 Then
	  ControlSend($MV, "", "", "{" & $HK & " down}")
   EndIf
   Sleep ($MA)
   ControlSend($MV, "", "", "{" & $MD & " up}")
   If $S = 1 Then
	  ControlSend($MV, "", "", "{" & $HK & " down}")
   EndIf
   If $H = 1 Then
	  ControlSend($MV, "", "", "{CTRLUP}")
   EndIf
   Sleep ($ST)
EndFunc

Func FN($r)
   While $MC<$r
		While $MC<=17
			ConsoleWrite($MC & @LF)
			$MC = $MC + 1
			Call(MB,28000,"a",1,0)

			$X = GUICtrlRead($TurnOff)
			if $x = 0 Then
				Call(StopAll)
				ExitLoop
			EndIf

			Call(MB,3000,"s",1,0)

			Call(MB,28000,"d",1,0)

			$X = GUICtrlRead($TurnOff)
			if $x = 0 Then
				Call(StopAll)
				ExitLoop
			EndIf

			Call(MB,3000,"s",1,0)

			Local $Remaining = $r - $MC
			GUICtrlSetData($AmountN,$Remaining)
		WEnd

		Call(MB,1000,"w",1,0)
		Call(MB,1000,"w",1,0)
		ConsoleWrite("Moved out of the teleporter!" & @LF)

		While $MC<$r
			$MC = $MC + 1
			Call(MB,28000,"d",1,0)
			$X = GUICtrlRead($TurnOff)
			if $x = 0 Then
				Call(StopAll)
				ExitLoop
			EndIf
			Call(MB,3000,"w",1,0)
			Call(MB,28000,"a",1,0)
			$X = GUICtrlRead($TurnOff)
			if $x = 0 Then
				Call(StopAll)
				ExitLoop
			EndIf
			Call(MB,3000,"w",1,0)

			Local $Remaining = $r - $MC
			GUICtrlSetData($AmountN,$Remaining)
		WEnd
   WEnd
EndFunc

Func StopAll()
   ControlSend($MV, "", "", "{w up}")
   ControlSend($MV, "", "", "{a up}")
   ControlSend($MV, "", "", "{s up}")
   ControlSend($MV, "", "", "{d up}")
   ControlSend($MV, "", "", "{x up}")
   Sleep(200)
   ControlSend($MV, "", "", "{x up}")
EndFunc

Func FCB($r,$d)

Local $Remaining = 0
local $Direction = ""

If $d = "left" Then
	$Direction = "a"
	ElseIf $d = "right" Then
	$Direction = "d"
EndIf

;--- Anstarten
Call(MB,7980,"w",1,1)

   While $MC<$r
	  $MC = $MC + 1

	  Call(MB,65,$Direction,0,1)

	  Call(MB,16185,"s",1,1)

	  $X = GUICtrlRead($TurnOff)
	  if $x = 0 Then
		 Call(StopAll)
		 ExitLoop
	  EndIf

	  $MC = $MC + 1

	  Call(MB,65,$Direction,0,1)

	  Call(MB,8050,"w",1,1)

	  $X = GUICtrlRead($TurnOff)
	  if $x = 0 Then
		 Call(StopAll)
		 ExitLoop
	  EndIf

	  Local $Remaining = $r - $MC
	  GUICtrlSetData($AmountCB,$Remaining)
   WEND
EndFunc