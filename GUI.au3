#include <GUIConstantsEx.au3>
#include <SendMessage.au3>
#include <WindowsConstants.au3>
#include <GuiEdit.au3>

#include "Test.au3"
#include "Farming.au3"

Global Const $SC_DRAGMOVE = 0xF012

Func GUI()
    Local $hGUI = GUICreate("GUI")
	GUICtrlCreateLabel							("Netherwarts", 7, 		7, 85)
    Local $iStartN 	= GUICtrlCreateButton		("Start", 		70, 	0, 85, 25)
	GUICtrlCreateLabel							("Reihen", 		160, 	7, 85, 25)
	Global $AmountN 	= GUICtrlCreateInput 	(70, 			200, 	0, 85, 25, $ES_CENTER)
	  GUICtrlSetLimit(-1, 3)
	Global $DirectionNW = GUICtrlCreateCombo  	("Left",		300, 	2, 85, 25)
    GUICtrlSetData($DirectionNW, "Right")

	GUICtrlCreateLabel							("CarrotsB", 	7, 		42, 	85)
    Local $iStartCB 	= GUICtrlCreateButton	("Start", 		70,		35, 	85, 25)
	GUICtrlCreateLabel							("Reihen", 		160, 	42, 	85, 25)
	Global $AmountCB 	= GUICtrlCreateInput 	(80,			200, 	35, 	85, 25, $ES_CENTER)
	  GUICtrlSetLimit(-1, 3)
    Global $DirectionCB = GUICtrlCreateCombo  	("Left",		300, 	37, 	85, 25)
    GUICtrlSetData($DirectionCB, "Right")

	GUICtrlCreateLabel							("Test", 		7, 		112, 	85)
    Local $Back 	= GUICtrlCreateButton		("Back", 		70, 	105, 	85, 25)
	GUICtrlCreateLabel							("To stop the script set textbox to 0 to start it again, set it back to 1", 		7, 		325, 	383)
	Global $TurnOff 	= GUICtrlCreateInput	(1,												7, 		340, 	382, 25, $ES_CENTER)
	Local $iClose 	= GUICtrlCreateButton		("Close", 										7, 		370, 	383, 25)
	  GUICtrlSetLimit(-1, 2)
	GUISetState(@SW_SHOW, $hGUI)

    While 1
        Switch GUIGetMsg()
            Case $GUI_EVENT_CLOSE, $iClose
                ExitLoop
			Case $iStartN
				$Amount = GUICtrlRead($AmountN)
				Call(FN,$Amount)
			Case $iStartCB
				$Amount = GUICtrlRead($AmountCB)
				$Direction = GUICtrlRead($DirectionCB)
				Call(FCB,$Amount,$Direction)
			Case $Back
            Case $GUI_EVENT_PRIMARYDOWN
                _SendMessage($hGUI, $WM_SYSCOMMAND, $SC_DRAGMOVE, 0)
        EndSwitch
    WEnd

    GUIDelete($hGUI)
 EndFunc

 GUI()
